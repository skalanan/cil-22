"""
Script for storing validation set needed for ensemble learning.
"""

from Baselines.utils import *
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-in_folder",
                    type=validate_file,
                    help="input data folder", 
                    default='Input/')
    args = parser.parse_args()

    train_pd, val_pd, test_pd = get_split_dataset(args.in_folder, args.seed)
    val_pd.to_csv('Output/validation.csv',index=False)
    #val_pd.to_csv('Output/validation2.csv',index=False)