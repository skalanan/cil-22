cd "$HOME/Downloads"
#sudo mkdir -p /opt/jdk
# java 8 download link obtained from: https://gist.github.com/wavezhang/ba8425f24a968ec9b2a8619d7c2d86a6
#sudo wget https://javadl.oracle.com/webapps/download/GetFile/1.8.0_321-b07/df5ad55fdd604472a86a45a217032c7d/linux-i586/jdk-8u321-linux-i586.tar.gz
#sha256sum jdk-8u321-linux-i586.tar.gz | awk '$1=="7074cf727cfd0cc81f75c15b09c5a9abda23cfd88c3afd38bb0102427778a522"{print"Java checksum correct"}'
#sudo tar -zxf jdk-8u321-linux-i586.tar.gz -C /opt/jdk/
#rm jdk-8u321-linux-i586.tar.gz
#sudo update-alternatives --install /usr/bin/java java /opt/jdk/jdk1.8.0_321/bin/java 1300
#sudo update-alternatives --install /usr/bin/javac javac /opt/jdk/jdk1.8.0_321/bin/javac 1300
#echo "export JAVA_HOME=/opt/jdk/jdk1.8.0_321" >> ~/.bashrc
#source ~/.bashrc 

wget https://archive.apache.org/dist/spark/spark-3.1.3/spark-3.1.3-bin-hadoop2.7.tgz
sha512sum spark-3.1.3-bin-hadoop2.7.tgz | awk '$1=="34B2C6C5 698C2541 81A020F9 E0E5D29B 3EDEB3B9 9CD7F511 05760B29 681461E6 E1BB0490 C09D8B20 05B5836F 22D74251 961939CA D010FFC0 D8DE656D 633B976E"{print"Spark checksum correct"}'
sudo mkdir -p /opt/spark
sudo tar xvf spark-3.1.3-bin-hadoop2.7.tgz -C /opt/spark
rm spark-3.1.3-bin-hadoop2.7.tgz
echo "export SPARK_HOME=/opt/spark/spark-3.1.3-bin-hadoop2.7" >> ~/.profile
echo "export PATH=$PATH:$SPARK_HOME/bin:$SPARK_HOME/sbin" >> ~/.profile
echo "export PYSPARK_PYTHON=/usr/bin/python3" >> ~/.profile
## EXECUTE source ~/.profile AFTER RUNNING THIS SCRIPT

#cp /opt/spark/spark-3.1.3-bin-hadoop2.7/conf/log4j.properties.template /opt/spark/spark-3.1.3-bin-hadoop2.7/conf/log4j.properties
#sed -i "s/=INFO/=ERROR/" /opt/spark/spark-3.1.3-bin-hadoop2.7/conf/log4j.properties