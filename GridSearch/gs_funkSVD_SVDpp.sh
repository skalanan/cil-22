#!/bin/bash

#This script performs a gridsearch in order to find suitable hyperparameters
#to perform funkSVD in combination with SVDpp on the given dataset


#Define Hyperparameters for the Grid Search


#Hyperparameters of the funkSVD models
#these parameters are hard coded containing configurations for some of the best funkSVD models
#this is done to reduce the complexity of the overall grid search
number_features=(15 10 15 15 15)
l_reg=(0.1 0.1 0.1 0.1 0.1)
learning_rate=(0.001 0.001 0.005 0.001 0.005)
n_iterations=(1500 1500 1000 1000 1500)


#Hyperparameters of the SVDpp models that should be decided on 
k_var=(15 20 25)
l_reg_bias_var=(0.01 0.015)
l_reg_var=(0.0015 0.002)
lr_bias_var=(0.001 0.002)
lr_var=(0.0001 0.0005)
i_var=(23 28 33)


#Variables used to monitor progress
curr=0
total_evaluations=$((${#k_var[@]} * ${#l_reg_bias_var[@]} * ${#l_reg_var[@]} * ${#lr_bias_var[@]} * ${#lr_var[@]} * ${#i_var[@]} * 5))

#Loop over the 6 predefined funkSVD models
for i in {0..4};
do

	#Execution of the model packaged into a python script
	#with varying hyperparameters
	python3 funkSVD.py -k ${number_features[i]} -l_reg ${l_reg[i]} -lr ${learning_rate[i]} -i ${n_iterations[i]} -in_folder ./Input -out_folder ./Output -perform_gs True -save_model_folder ./Input/Model
					
	#Perform grid search over all hyperparemeters for the SVDpp model				
	for k_ in "${k_var[@]}";
	do
		for l_reg_bias_ in "${l_reg_bias_var[@]}";
		do
			for l_reg_ in "${l_reg_var[@]}";
			do
				for lr_bias_ in "${lr_bias_var[@]}";
				do
					for lr_ in "${lr_var[@]}";
					do
						for i_ in "${i_var[@]}";
						do
							#Execution of the model packaged into a python script
							#with varying hyperparameters
							python3 SVDpp.py -k $k_ -l_reg_bias $l_reg_bias_ -l_reg $l_reg_ -lr_bias $lr_bias_ -lr $lr_ -i $i_ -preinit_folder ./Input/Model -in_folder ./Input -out_folder ./Output -perform_gs True
							
							#wait for the executing python script to finish
							wait
							
							
							#output progress oof the grid search
							#current iteration and total number of iterations
							curr=$((curr+1))
							echo "Progress $curr / $total_evaluations"
							
							#Read achived validation loss by the model from the file created by the script
							#and save the validation loss and hyperparameters in a .txt file
							while read -r line; do
								echo "Validation loss: ${line}, FunkSVD parameters: k: ${number_features[i]}, l_reg: ${l_reg[i]}, -lr: ${learning_rate[i]}, -i: ${n_iterations[i]}, SVDpp parameters: k: ${k_}, l_reg_bias: ${l_reg_bias_}, lreg: ${l_reg_}, lr_bias: ${lr_bias_}, lr: ${lr_}, i: ${i_}" >> ./Output/grid_search/gs_funkSVD_SVDpp.txt
							done < ./Input/temp_gs.txt
							
							#delete the file containing the validation loss of the last model configuration
							rm  ./Input/temp_gs.txt
							
						done
					done
				done
			done
		done
	done
	
	
	
	

done


