# Grid Search Scripts

There is one script for every baseline.

Execute them in the root folder of the project executing 
   ```./GridSearch/gs_{name of baseline}.sh``` 
   from the terminal.
You might need to execute ```chmod +x ./GridSearch/gs_{name of baseline}.sh``` first in order to be able to run them.

The results are then written into ```./Output/grid_search/gs_{name of baseline}.txt```.

It includes the validation score and the parameters used.

Further more gs_extract.py extracts the selected amount of best performing configurations from a grid search result file

It has to be executed from the root folder of the project
	```python3 ./GridSearch/gs_extract.py -n {number of configuration wanted} --path_and_filename ./Output/grid_search/gs_{name of model}.txt -out_folder_file_name ./Output/grid_search/gs_best_10_{name of model}.txt``` 

The results are then written to ```./Output/grid_search/gs_best_10_{name of model}.txt```
