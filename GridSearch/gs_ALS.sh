#!/bin/bash

#This script performs a gridsearch in order to find suitable hyperparameters
#to perform ALS on the given dataset


#Define Hyperparameters for the Grid Search
k_var=(5 10 15)
l_reg_var=(0.001 0.005 0.01)
i_var=(15 19 23)


#Variables used to monitor progress
curr=0
total_evaluations=$((${#k_var[@]}  * ${#l_reg_var[@]} * ${#i_var[@]} ))

#Loop over all hyperparameters				
for k_ in "${k_var[@]}";
do
	for l_reg_ in "${l_reg_var[@]}";
	do
		
		for i_ in "${i_var[@]}";
		do
		
		#Execution of the model packaged into a python script
		#with varying hyperparameters
		python3 ./Baselines/als.py -k $k_ -l_reg $l_reg_ -i $i_ -in_folder ./Input -out_folder ./Output -perform_gs True
									
		#wait for the executing python script to finish
		wait
				
		#output progress oof the grid search
		#current iteration and total number of iterations
		curr=$((curr+1))			
		echo "Progress $curr / $total_evaluations"
		
		#Read achived validation loss by the model from the file created by the script
		#and save the validation loss and hyperparameters in a .txt file			
		while read -r line; do
		 	echo "Validation loss: ${line}, k: ${k_},  l-reg: ${l_reg_}, -i: ${i_}" >> ./Output/grid_search/gs_ALS.txt
		done < ./Input/temp_gs_als.txt
						
						
		#delete the file containing the validation loss of the last model configuration
		rm  ./Input/temp_gs_als.txt
		
		
								
		done
	done
	
done
	

