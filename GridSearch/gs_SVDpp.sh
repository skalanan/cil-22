#!/bin/bash

#This script performs a gridsearch in order to find suitable hyperparameters
#for the SVDpp model on the given dataset


#Define Hyperparameters for the Grid Search
k_var=(25)
l_reg_bias_var=(0.005 0.01)
l_reg_var=(0.0015 0.002)
lr_bias_var=(0.001)
lr_var=(0.001 0.0001)
i_var=(50 100 250 500)

#Variables used to monitor progress
curr=0
total_evaluations=$((${#k_var[@]} * ${#l_reg_bias_var[@]} * ${#l_reg_var[@]} * ${#lr_bias_var[@]} * ${#lr_var[@]} * ${#i_var[@]}))


#Loop over all hyperparameters			
for k_ in "${k_var[@]}";
do
	for l_reg_bias_ in "${l_reg_bias_var[@]}";
	do
		for l_reg_ in "${l_reg_var[@]}";
		do
			for lr_bias_ in "${lr_bias_var[@]}";
			do
				for lr_ in "${lr_var[@]}";
				do
					for i_ in "${i_var[@]}";
					do
					
						#Execution of the model packaged into a python script
						#with varying hyperparameters
						
						python3 ./Baselines/SVDpp.py -k $k_ -l_reg_bias $l_reg_bias_ -l_reg $l_reg_ -lr_bias $lr_bias_ -lr $lr_ -i $i_ -in_folder ./Input -out_folder ./Output -perform_gs True
						
						#wait for the executing python script to finish
						wait
						
						
						#output progress oof the grid search
						#current iteration and total number of iterations
						curr=$((curr+1))
						echo "Progress $curr / $total_evaluations"
						
						#Read achived validation loss by the model from the file created by the script
						#and save the validation loss and hyperparameters in a .txt file
						while read -r line; do
							echo "Validation loss: ${line}, k: ${k_}, l_reg_bias: ${l_reg_bias_}, l_reg: ${l_reg_}, lr_bias: ${lr_bias_}, lr: ${lr_}, i: ${i_}" >> ./Output/grid_search/gs_SVDpp.txt
						done < ./Input/temp_gs_svdpp.txt
						#delete the file containing the validation loss of the last model configuration
						rm  ./Input/temp_gs_svdpp.txt							
					done
				done
			done
		done
	done
done
	

