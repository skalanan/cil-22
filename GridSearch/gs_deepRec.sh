#!/bin/bash

#This script performs a gridsearch in order to find suitable hyperparameters
#for the deepRec model on the given dataset


#Define Hyperparameters for the Grid Search
k_var=(64 128 150)
lr_var=(0.005 0.007 0.009)
batch_size_var=(64)
dropout_var=(0.4 0.55 0.7)
epochs_var=(500 1000 1500)


#Variables used to monitor progress
curr=0
total_evaluations=$((${#k_var[@]}  * ${#lr_var[@]} * ${#batch_size_var[@]} * ${#dropout_var[@]} * ${#epochs_var[@]} ))

#Loop over all hyperparameters				
for k_ in "${k_var[@]}";
do
	for lr_ in "${lr_var[@]}";
	do
		
		for batch_ in "${batch_size_var[@]}";
		do
			for dropout_ in "${dropout_var[@]}";
			do
				for epochs_ in "${epochs_var[@]}";
				do
				
		
				#Execution of the model packaged into a python script
				#with varying hyperparameters
				python3 ./Baselines/deepRec.py -k $k_ -lr $lr_ -batch_size $batch_ -dropout $dropout_ -epochs $epochs_ -in_folder ./Input -out_folder ./Output -perform_gs True -val_folder ./Output/validation_pred
											
				#wait for the executing python script to finish
				wait
						
				#output progress oof the grid search
				#current iteration and total number of iterations
				curr=$((curr+1))			
				echo "Progress $curr / $total_evaluations"
				
				#Read achived validation loss by the model from the file created by the script
				#and save the validation loss and hyperparameters in a .txt file			
				while read -r line; do
				 	echo "Validation loss: ${line}, k: ${k_}, lr: ${lr_}, batch_size: ${batch_}, dropout: ${dropout_}, epochs: ${epochs_}" >> ./Output/grid_search/gs_deepRec.txt
				done < ./Input/temp_gs_deepRec.txt
								
								
				#delete the file containing the validation loss of the last model configuration
				rm  ./Input/temp_gs_deepRec.txt
				
				
				done
			done	
								
		done
	done
	
done
