#example call
#python3 ./GridSearch/gs_extract.py -n 10 -path_and_filename ./Output/grid_search/gs_funkSVD.txt -out_folder_file_name ./Output/grid_search/gs_best_10_funkSVD.txt

import numpy as np
import os
import argparse
 
 
#Helper function extracts validation loss from a line in a .txt file
def read_validation_loss(var_string):
	split_line = var_string.split()
	"""for i in split_line[0].split():
		try:
			#trying to convert i to float
			print(i)
			result = float(i)
			#break the loop if i is the first string that's successfully converted
		except:
			result = 100
	"""
	return float(split_line[2][:-1]);
        		
if __name__ == "__main__":
	# PARSE ARGUMENTS
	parser = argparse.ArgumentParser()
	parser.add_argument('-n',
		type=int,
		help='number configurations',
		default=10)
	parser.add_argument('-path_and_filename',
		type=str,
		help='path to Grid search file')
	parser.add_argument('-out_folder_file_name',
		type=str,
		help='path to the results')
	args = parser.parse_args()
	
	#open the file where gridsearch findings are stored
	with open(os.path.join(args.path_and_filename)) as f:
	
		#array to store best validation losses
		array = np.empty(0, dtype=float)
		
		#define array where the best configuration strings are beeing stored
		string_final=[]
		for i in range(args.n):
			string_final.append(None)
		
		#extract validation loss for all configurations
		for line in f:
			config_loss = read_validation_loss(line)
			array = np.append(array, config_loss)
		
		#take only the n best validation losses
		array = np.sort(array)[0:args.n]
	
	#retrive the lines for the best n configurations and store them according to there performance
	with open(os.path.join(args.path_and_filename)) as f:
		for line in f:
			config_loss = read_validation_loss(line)
			if config_loss in array:
				string_final[np.where(array == config_loss)[0][0]] = line
	
	#write the strings into a file
	with open(os.path.join(args.out_folder_file_name),'w') as e:
		for i in range(len(string_final)):
			e.write(string_final[i])
			
				
	
                       
                       




