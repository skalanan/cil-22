#!/bin/bash

#This script performs a gridsearch in order to find suitable hyperparameters
#for the SVD model on the given dataset

#Define Hyperparameters for the Grid Search
number_features=(5 10 15 20 25 50 100 150 200)

#Variables used to monitor progress
total_evaluations=$((${#number_features[@]}))
echo $total_evaluations

#Loop over all hyperparameters	
for nf in "${number_features[@]}";
do 	
	#Execution of the model packaged into a python script
	#with varying hyperparameters
	python3 ./Baselines/svd.py -k $nf -in_folder ./Input -out_folder ./Output -perform_gs True
	
	#wait for the executing python script to finish
	wait
	
	#output progress oof the grid search
	#current iteration and total number of iterations
	curr=$((curr+1))
	echo "Progress $curr / $total_evaluations"
	
	
	
	#Read achived validation loss by the model from the file created by the script
	#and save the validation loss and hyperparameters in a .txt file
	while read -r line; do
		echo "Validation loss: ${line}, k: ${nf}" >> ./Output/grid_search/gs_svd.txt
	done < ./Input/temp_gs_svd.txt
	
	#delete the file containing the validation loss of the last model configuration
	rm  ./Input/temp_gs_svd.txt
done



