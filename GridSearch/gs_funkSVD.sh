#!/bin/bash


#This script performs a gridsearch in order to find suitable hyperparameters
#for the funk SVD model on the given dataset


#Define Hyperparameters for the Grid Search
number_features=(5 10 15)
l_reg=(0.05 0.1 0.15)
learning_rate=(0.005 0.001 0.015)
n_iterations=( 500 1000 1500)



#Variables used to monitor progress
total_evaluations=$((${#number_features[@]} * ${#l_reg[@]} * ${#learning_rate[@]} * ${#n_iterations[@]}))
echo $total_evaluations

#Loop over all hyperparameters	
for nf in "${number_features[@]}";
do 
	for lreg in "${l_reg[@]}";
	do
		for lr in "${learning_rate[@]}";
		do
			for iter in "${n_iterations[@]}"; 
			do
				
				#Execution of the model packaged into a python script
				#with varying hyperparameters
				python3 ./Baselines/funkSVD.py -k $nf -l_reg $lreg -lr $lr -i $iter -in_folder ./Input -out_folder ./Output -perform_gs True
				
				#wait for the executing python script to finish
				wait
				
				#output progress oof the grid search
				#current iteration and total number of iterations
				curr=$((curr+1))
				echo "Progress $curr / $total_evaluations"
				
				
				
				#Read achived validation loss by the model from the file created by the script
				#and save the validation loss and hyperparameters in a .txt file
				while read -r line; do
					echo "Validation loss: ${line}, k: ${nf}, l_reg: ${lreg}, lr: ${lr}, i:s ${iter}" >> ./Output/grid_search/gs_funkSVD.txt
				done < ./Input/temp_gs_funkSVD.txt
				
				#delete the file containing the validation loss of the last model configuration
				rm  ./Input/temp_gs_funkSVD.txt
				
			done
		done
	done
done



