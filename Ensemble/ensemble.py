import os
import argparse
import pandas as pd
import numpy as np
from numba import njit
from sklearn.model_selection import train_test_split
from pathlib import Path
from sklearn.linear_model import LogisticRegression
from sklearn import linear_model, svm, tree
import sklearn
from sklearn.ensemble import AdaBoostRegressor
from sklearn.linear_model import RidgeCV
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler

def validate_file(location):
    if not os.path.exists(location):
        raise argparse.ArgumentTypeError("{0} does not exist".format(location))
    return location

def extract_users_items_predictions(data_pd):
    users, movies = \
        [np.squeeze(arr) for arr in \
             np.split(data_pd.Id.str.extract('r(\d+)_c(\d+)').values.astype(int) - 1, 2, axis=-1)]
    predictions = data_pd.Prediction.values
    return users, movies, predictions

def clip(num):
    return num + 10

if __name__ == "__main__":
    print("Starting")

    # PARSE ARGUMENTS
    parser = argparse.ArgumentParser()
    parser.add_argument("-val_pred_folder",
                        type=validate_file,
                        help="input data folder", 
                        default='Output/validation_pred')
    parser.add_argument("-test_pred_folder",
                        type=validate_file,
                        help="test data folder", 
                        default='Output/test_pred')
    parser.add_argument("-models",
                        type=str,
                        help="models folder name", 
                        default='SVDpp+FunkSVD+ALS+WSVD++')
    parser.add_argument("-validation_file",
                        type=validate_file,
                        help="test data folder", 
                        default='Output/validation.csv')
    parser.add_argument("-alpha",
                        type=float,
                        help="test data folder", 
                        default='17')

    args = parser.parse_args()
    
    val_files = list(Path(os.path.join(args.val_pred_folder,args.models)).glob('*.csv'))
    val_first = pd.read_csv(val_files[0])
    val_pred = val_first['Prediction']
    print(val_files[0])
    for val_file in val_files[1:]:
        print(val_file)
        if 'als' not in str(val_file): 
            val_pd = pd.read_csv(val_file)['Prediction']
        else:
            # need to reindex for als
            val_pd = pd.read_csv(val_file)
            val_pd = val_pd.set_index('Id')
            val_pd = val_pd.reindex(index=val_first['Id'])
            val_pd = val_pd.reset_index()['Prediction']

        val_pred=pd.concat([val_pred, val_pd],axis=1)

    test_files = list(Path(os.path.join(args.test_pred_folder,args.models)).glob('*.csv'))
    test_first = pd.read_csv(test_files[0])
    test_pred = test_first['Prediction']
    
    for test_file in test_files[1:]:
        if 'als' not in str(test_file): 
            test_pd = pd.read_csv(test_file)['Prediction']
        else:
            # need to reindex for als
            test_pd = pd.read_csv(test_file)
            test_pd = test_pd.set_index('Id')
            test_pd = test_pd.reindex(index=test_first['Id'])
            test_pd = test_pd.reset_index()['Prediction']

        test_pred = pd.concat([test_pred, test_pd],axis=1)

    val_ground_truth = pd.read_csv(args.validation_file)['Prediction']
    
    # Ridge Regression
    model = linear_model.Ridge(alpha=args.alpha)
    model.fit(val_pred,val_ground_truth)
    model_pred = model.predict(test_pred)
    model_pred = np.clip(model_pred, 1, 5)
    weights = model.coef_
    np.save(args.models+'.npy', weights)

    test_id = pd.read_csv(test_files[0])['Id']
    d = {'Id': test_id, 'Prediction': model_pred}
    df = pd.DataFrame(data=d)
    df.to_csv('Output/ensemble_pred/ensemble_'+args.models+'.csv', index=False)
    
    """
    X_train, X_test, y_train, y_test = train_test_split(val_pred, val_ground_truth, test_size=0.1, random_state=0)    
    
    #r_alphas = [1e-30,1e-8, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 0.5, 0.75, 1, 1.25, 1.5, 2, 2.5, 3, 3.5, 3.55, 3.57, 3.58, 15, 30, 50, 60, 70, 80, 90, 100, 500, 100]  
    r_alphas = np.arange(15,30,1)
    
    best_rmse = 100
    best_alpha = 0
    for alpha in r_alphas: 
        model = linear_model.Ridge(alpha=alpha)
        model.fit(X_train,y_train)
        y_pred = model.predict(X_test)
        y_pred = np.clip(y_pred, 1, 5)
        rmse = np.sqrt(mean_squared_error(y_test,y_pred))
        if (rmse < best_rmse):
            best_rmse = rmse
            best_alpha = alpha
    
    print("best alpha: ", best_alpha, best_rmse)
    """