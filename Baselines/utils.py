"""
Helper methods for calculating the score, reading in, preprocessing data,
extracting predictions and writing files.
A lot of these methods are taken from the project 1 jupyter 
notebook of the ETH CIL 2021 course:
https://github.com/dalab/lecture_cil_public/blob/master/exercises/2021/Project_1.ipynb
"""
from cgi import test
import os
import math
import argparse
import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split

DEFAULT_SEED = 42
DEFAULT_SPLIT = 0.9

TRAIN_DATA_FILE = 'data_train.csv'
TEST_DATA_FILE = 'sampleSubmission.csv'
RECONSTRUCTION_FILE = 'reconstruction.csv'

n_users, n_movies = (10000, 1000)

def validate_file(location):
    if not os.path.exists(location):
        raise argparse.ArgumentTypeError("{0} does not exist".format(location))
    return location

rmse = lambda x, y: math.sqrt(mean_squared_error(x, y))

def get_score(predictions, target_values):
    return rmse(predictions, target_values)

def get_split_dataset(folder, seed, no_split=False):
    split = 0 if no_split else DEFAULT_SPLIT
    data_pd = pd.read_csv(os.path.join(folder, TRAIN_DATA_FILE))
    test_pd = pd.read_csv(os.path.join(folder, TEST_DATA_FILE))
    if no_split:
        return data_pd, pd.DataFrame(), test_pd
    else: 
        train_pd, val_pd = train_test_split(data_pd, train_size=split, random_state=seed)
        return train_pd, val_pd, test_pd

def extract_users_items_predictions(data_pd):
    if data_pd.empty:
        return None, None, None
    else:
        users, movies = \
            [np.squeeze(arr) for arr in \
                np.split(data_pd.Id.str.extract('r(\d+)_c(\d+)').values.astype(int) - 1, 2, axis=-1)]
        predictions = data_pd.Prediction.values
        return users, movies, predictions
        

def save_to_file(reconstruction, folder_path, filename):
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    np.savetxt(os.path.join(folder_path, filename), reconstruction, delimiter=',')