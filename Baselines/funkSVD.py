#!/usr/bin/env python3.7
"""
Funk SVD

Reference implementation which this is based on can be found
in Geoffry Bolmier's github repository: https://github.com/gbolmier/funk-svd
This implementation is almost fully equivalent to Geoffry Bolmier's implementation 
up to some renamings.
"""

import os
import logging
import argparse
import numpy as np
from numba import njit
from utils import *

USER_FEATURES_FILE = 'user_features.csv'
MOVIE_FEATURES_FILE = 'movie_features.csv'
USER_BIAS_FILE = 'user_bias.csv'
MOVIE_BIAS_FILE = 'movie_bias.csv'

@njit # njit can be very helpful (google it), see reference for what python/numpy constructs are supported
def initialize(n_features, seed):
    np.random.seed(seed)
    user_features = np.random.normal(0, .1, (n_users, n_features))
    movie_features = np.random.normal(0, .1, (n_movies, n_features))
    user_bias = np.zeros(n_users)
    movie_bias = np.zeros(n_movies)
    return user_bias, movie_bias, user_features, movie_features

@njit
def iterate(train_users,train_movies,train_ratings, 
            mean_rating, user_bias, movie_bias, user_features, movie_features,
            learning_rate, lambda_reg):
    train_residuals = np.zeros(len(train_ratings))
    for i,(user,movie,rating) in enumerate(zip(train_users,train_movies,train_ratings)):
        prediction = mean_rating + user_bias[user] + movie_bias[movie]
        for feature in range(n_features):
            prediction += user_features[user, feature] * movie_features[movie, feature]
        residual = rating - prediction
        train_residuals[i] = residual
        user_bias[user] += learning_rate * (residual - lambda_reg * user_bias[user])
        movie_bias[movie] += learning_rate * (residual - lambda_reg * movie_bias[movie])
        for feature in range(n_features):
            user_feature = user_features[user, feature]
            movie_feature = movie_features[movie, feature]
            user_features[user, feature] += learning_rate * \
                                    (residual * movie_feature - lambda_reg * user_feature)
            movie_features[movie, feature] += learning_rate * \
                                    (residual * user_feature - lambda_reg * movie_feature)
    train_rmse = np.sqrt(np.square(train_residuals).mean())
    return user_bias, movie_bias, user_features, movie_features, train_rmse

@njit
def get_validation_rmse(val_users, val_movies, val_ratings,
            mean_rating, user_bias, movie_bias, user_features, movie_features):
    val_residuals = np.zeros(len(val_ratings))
    for i, (user, movie, rating) in enumerate(zip(val_users, val_movies, val_ratings)):
        prediction = mean_rating + user_bias[user] + movie_bias[movie]
        for feature in range(n_features):
            prediction += user_features[user, feature] * movie_features[movie, feature]
        val_residuals[i] = rating - prediction
    return np.sqrt(np.square(val_residuals).mean())

def save_model(user_features, 
                movie_features,
                user_bias, movie_bias,
                folder):
    save_to_file(user_features,folder,USER_FEATURES_FILE)
    save_to_file(movie_features,folder,MOVIE_FEATURES_FILE)
    save_to_file(user_bias,folder,USER_BIAS_FILE)
    save_to_file(movie_bias,folder,MOVIE_BIAS_FILE)

if __name__ == "__main__":
    # PARSE ARGUMENTS
    parser = argparse.ArgumentParser()
    parser.add_argument('-k',
                       type=int,
                       help='number of features',
                       default=10)
    parser.add_argument('-l_reg',
                       type=float,
                       help='regularizer lambda',
                       default=0.1)
    parser.add_argument('-lr',
                        type=float,
                        help='learning rate',
                        default=0.001)
    parser.add_argument('-i',
                        type=int,
                        help='number of iterations',
                        default=1000)
    parser.add_argument('-seed',
                        type=int,
                        help='set seed for reproducibility',
                        default=DEFAULT_SEED)
    parser.add_argument('-preinit_folder',
                        type=validate_file,
                        help="preinitialization files folder")
    parser.add_argument('-save_model_folder',
                        help="path to folder where model should be saved")
    parser.add_argument('-save_reconstruction_folder',
                        help="folder where to save reconstruction")
    parser.add_argument("-in_folder",
                        type=validate_file,
                        help="input data folder", 
                        default='Input/')
    parser.add_argument("-out_folder",
                        type=validate_file,
                        help="output folder", 
                        default='Output/test_pred')
    parser.add_argument("-val_folder",
                        type=validate_file,
                        help="output folder", 
                        default='Output/validation_pred')
    parser.add_argument("-no_val_split",
                        help="train on full data set, don't split for validation",
                        action='store_true')
    parser.add_argument("-perform_gs",
                        type=bool,
                        help="generates file for gs", 
                        default=False)
    parser.add_argument('-v', '--verbose',
                        help="displays output (logging at info level)",
                        action="store_const", 
                        dest="loglevel", 
                        const=logging.INFO,
                        default=logging.CRITICAL)             
    args = parser.parse_args()
    n_features = args.k
    lambda_reg = args.l_reg
    learning_rate = args.lr
    n_iterations = args.i
    logging.getLogger().setLevel(args.loglevel)

    logging.info('Starting')

    # READ DATA IN
    train_pd, val_pd, test_pd = get_split_dataset(args.in_folder, args.seed, args.no_val_split)
    train_users, train_movies, train_ratings = extract_users_items_predictions(train_pd)
    val_users, val_movies, val_ratings = extract_users_items_predictions(val_pd)
    test_users, test_movies, _ = extract_users_items_predictions(test_pd)

    # INITIALIZE
    logging.info("Initialize")
    mean_rating = train_ratings.mean()
    user_bias, movie_bias, user_features, movie_features = initialize(n_features, args.seed)
    if args.preinit_folder:
        precomp_n_features = 0
        with open(os.path.join(args.preinit_folder, USER_FEATURES_FILE)) as f:
            first_line = f.readline()
            precomp_n_features = len(first_line.split(','))
        min_n_features = min(precomp_n_features, n_features)
        user_features[0:,0:min_n_features] = np.loadtxt(os.path.join(args.preinit_folder, USER_FEATURES_FILE), delimiter=',')[0:,0:min_n_features]
        movie_features[0:,0:min_n_features] = np.loadtxt(os.path.join(args.preinit_folder, MOVIE_FEATURES_FILE), delimiter=',')[0:,0:min_n_features]
        user_bias = np.loadtxt(os.path.join(args.preinit_folder, USER_BIAS_FILE), delimiter=',')
        movie_bias = np.loadtxt(os.path.join(args.preinit_folder, MOVIE_BIAS_FILE), delimiter=',')

    # ITERATE
    logging.info("Iterate")
    for i in range(n_iterations):
        user_bias, movie_bias, user_features, movie_features, train_rmse = \
                iterate(train_users, train_movies, train_ratings,
                        mean_rating, user_bias, movie_bias, user_features, movie_features,
                        learning_rate, lambda_reg)  
        if args.no_val_split:  
            logging.info("Iteration {}/{}".format(i+1, n_iterations))
        elif args.loglevel < logging.CRITICAL:
            val_rmse = get_validation_rmse(val_users, val_movies, val_ratings,
                                mean_rating, user_bias, movie_bias, user_features, movie_features)
            logging.info("Iteration {}/{}:\t[ train_rmse: {:.4f}, validation rmse: {:.4f} ]" \
                .format(i+1, n_iterations, train_rmse, val_rmse))

    # PREDICT + WRITE OUT TEST SET
    if args.out_folder:
        logging.info('Predict test set')
        if args.no_val_split:
            filename = 'funk_svd_submission_no_val_split.csv'
        else:
            filename = 'funk_svd_test_submission.csv'
        with open(os.path.join(args.out_folder,filename), 'w') as f:
            f.write('Id,Prediction\n')
            for(user,movie) in zip (test_users,test_movies):
                prediction = mean_rating + user_bias[user] + movie_bias[movie] \
                            + np.dot(user_features[user], movie_features[movie])
                prediction = min(max(1, prediction), 5) # clip
                f.write("r{}_c{},{}\n".format(user+1,movie+1,prediction))
    
    if args.no_val_split:
        logging.warning("Cannot write gradient search file or validation predictions when -no_val_split option is set")
    else:
        # WRITE GRADIENT SEARCH FILE
        if args.perform_gs:
            final_val_rmse = get_validation_rmse(val_users, val_movies, val_ratings,
                                    mean_rating, user_bias, movie_bias, user_features, movie_features)
            np.savetxt(os.path.join(args.in_folder, 'temp_gs_funkSVD.txt'), [final_val_rmse])
        # PREDICT + WRITE OUT VALIDATION SET
        if args.val_folder:
                logging.info('Predict validation set')
                with open(os.path.join(args.val_folder,'funk_svd_val_submission.csv'), 'w') as f:
                    f.write('Id,Prediction\n')
                    for(user,movie,rating) in zip (val_users,val_movies, val_ratings):
                        prediction = mean_rating + user_bias[user] + movie_bias[movie] \
                                    + np.dot(user_features[user], movie_features[movie])
                        prediction = min(max(1, prediction), 5) # clip
                        f.write("r{}_c{},{}\n".format(user+1,movie+1,prediction))

    # SAVE MODEL
    if args.save_model_folder:
        logging.info('Save model')
        save_model(user_features, 
                movie_features,
                user_bias, 
                movie_bias,
                args.save_model_folder)
    
    # SAVE RECONSTRUCTED MATRIX
    if args.save_reconstruction_folder:
        logging.info("Save reconstructed matrix")
        reconstruction = user_features @ movie_features.T \
                        + np.expand_dims(user_bias,1)  \
                        + np.expand_dims(movie_bias,0) \
                        + mean_rating
        reconstruction = np.where(reconstruction > 5, 5, reconstruction)
        reconstruction = np.where(reconstruction < 1, 1, reconstruction)
        save_to_file(reconstruction, 
                args.save_reconstruction_folder, 
                RECONSTRUCTION_FILE)

    logging.info("Finished")
