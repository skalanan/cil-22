#!/usr/bin/env python3.7
"""
DeepRec

Autoencoder implementation based on the paper 'Training Deep AutoEncoders for Recommender Systems'
by Oleksii Kuchaiev and Boris Ginsburg. The autoencoder implementation of the project 1 jupyter 
notebook of the ETH CIL 2021 course served as a base implementation and was improved with the ideas
from the paper. The notebook can be found at:
https://github.com/dalab/lecture_cil_public/blob/master/exercises/2021/Project_1.ipynb
"""

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, TensorDataset
from torch.utils.tensorboard import SummaryWriter

class Encoder(nn.Module):
    def __init__(self, input_dimension, encoded_dimension=128, dropout=0.55):
        super().__init__()

        self.model = nn.Sequential(
            # Noise(),
            nn.Linear(in_features=input_dimension, out_features=64),
            nn.SELU(),
            nn.Linear(in_features=64, out_features=128),
            nn.SELU(),
            nn.Linear(in_features=128, out_features=encoded_dimension),
            nn.SELU(),
            nn.Dropout(dropout)
        )
    
    def forward(self, data):
        return self.model(data)

class Decoder(nn.Module):
    def __init__(self, output_dimensions, encoded_dimension=128):
        super().__init__()

        self.model = nn.Sequential(
            nn.Linear(in_features=encoded_dimension, out_features=128),
            nn.SELU(),
            nn.Linear(in_features=128, out_features=64),
            nn.SELU(),
            nn.Linear(in_features=64, out_features=output_dimensions),
            nn.SELU(),
        )
    
    def forward(self, data):
        return self.model(data)


class AutoEncoder(nn.Module):
    def __init__(self, encoder, decoder):
        super().__init__()
        self.encoder = encoder
        self.decoder = decoder

    def forward(self, data):
        return self.decoder(self.encoder(data))


import os
import logging
import argparse
import enlighten
import numpy as np
from utils import *

AUTOENCODER_MODEL_FILE = 'autoEncoderModel'

def extract_prediction_from_full_matrix(reconstructed_matrix, users, movies):
    predictions = np.zeros(len(users))

    for i, (user, movie) in enumerate(zip(users, movies)):
        predictions[i] = reconstructed_matrix[user][movie]

    return predictions

def loss_function(original, reconstructed, mask):
    return torch.mean(mask * (original - reconstructed) ** 2)

def reconstruct_whole_matrix(autoencoder,data_torch, batch_size):
    data_reconstructed = np.zeros((n_users, n_movies))
    
    with torch.no_grad():
        for i in range(0, n_users, batch_size):
            upper_bound = min(i + batch_size, n_users)
            data_reconstructed[i:upper_bound] = autoencoder(data_torch[i:upper_bound]).detach().cpu().numpy()

    return data_reconstructed

def initialize_weights(layer):
  if isinstance(layer, nn.Linear):
    torch.nn.init.xavier_uniform_(layer.weight)

"""
def write_final_prediction(predictions,test_users,test_movies):
    with open('submission.csv', 'w') as f:
        f.write('Id,Prediction\n')
        for(user,movie,pred) in zip (test_users,test_movies,predictions):
            f.write("r{}_c{},{}\n".format(user+1,movie+1,pred))"""

if __name__ == "__main__":
    # PARSE ARGUMENTS
    parser = argparse.ArgumentParser()
    parser.add_argument('-k',
                       type=int,
                       help='number of features (encoded dimension)',
                       default=128)
    parser.add_argument('-lr',
                        type=float,
                        help='learning rate',
                        default=0.007)
    parser.add_argument('-batch_size',
                        type=int,
                        help='number of samples in batch',
                        default=64)
    parser.add_argument('-dropout',
                        type=float,
                        help="Dropout after encoder for regularization",
                        default=0.55)
    parser.add_argument('-epochs',
                        type=int,
                        help='number of training epochs',
                        default=1500)
    parser.add_argument('-seed',
                        type=int,
                        help='set seed for reproducibility',
                        default=DEFAULT_SEED)
    parser.add_argument('-refeed_rec_folder',
                        type=validate_file,
                        help="path the folder of reconstruction used for refeeding")
    parser.add_argument('-preinit_folder',
                        type=validate_file,
                        help="preinitialization files folder")
    parser.add_argument('-save_model_folder',
                        help="path to folder where model should be saved")
    #parser.add_argument('-save_reconstruction_folder',
    #                    help="folder where to save reconstruction")
    parser.add_argument("-in_folder",
                        type=validate_file,
                        help="input data folder", 
                        default='Input/')
    parser.add_argument("-val_folder",
                        type=validate_file,
                        help="validation output folder", 
                        default='Output/validation_pred')
    parser.add_argument("-no_val_split",
                        help="train on full data set, don't split for validation",
                        action='store_true')
    parser.add_argument("-out_folder",
                        type=validate_file,
                        help="test output folder", 
                        default='Output')
    parser.add_argument("-log_dir",
                        help="directory where tensorboard logs can be found", 
                        default='./Baselines/tensorboard/autoencoder')
    parser.add_argument("-show_validation_score_every_epochs",
                        type=int, 
                        help="number of epochs which pass until validation score is refreshed",
                        default = 1)
    parser.add_argument("-perform_gs",
                        type=bool,
                        help="generates file for gs", 
                        default=False)
    parser.add_argument('-v', '--verbose',
                        help="displays output (logging at info level)",
                        action="store_const", 
                        dest="loglevel", 
                        const=logging.INFO,
                        default=logging.CRITICAL)
    args = parser.parse_args()
    encoded_dimension = args.k
    learning_rate = args.lr
    batch_size = args.batch_size
    n_epochs = args.epochs
    show_validation_score_every_epochs = args.show_validation_score_every_epochs
    logging.getLogger().setLevel(args.loglevel)

    logging.info("Starting")

    # READ DATA IN
    train_pd, val_pd, test_pd = get_split_dataset(args.in_folder, args.seed, args.no_val_split)
    train_users, train_movies, train_ratings = extract_users_items_predictions(train_pd)
    val_users, val_movies, val_ratings = extract_users_items_predictions(val_pd)
    test_users, test_movies, _ = extract_users_items_predictions(test_pd)

    # INITIALIZE
    logging.info("Initialize")

    # Set seed for reproducibility
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)

    # Get device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Build Autoencoder
    encoder=Encoder(
        input_dimension=n_movies,
        encoded_dimension=encoded_dimension,
        dropout=args.dropout
    )
    decoder=Decoder(
            output_dimensions=n_movies,
            encoded_dimension=encoded_dimension,
    )
    autoencoder = AutoEncoder(
        encoder=encoder,
        decoder=decoder
    ).to(device)

    # SGD optimizer
    optimizer = optim.SGD(autoencoder.parameters(),
                      lr=learning_rate, momentum=0.9)

    # Preinitialize weights
    if args.preinit_folder:
        autoencoder.load_state_dict(
            torch.load(AUTOENCODER_MODEL_FILE, 
            map_location=torch.device(device))
            )
    else:
        autoencoder.apply(initialize_weights)

    # Build Dataloaders
    data = np.full((n_users, n_movies), np.nan)
    data = np.where(np.isnan(data), np.ma.array(data, mask=np.isnan(data)).mean(axis=0), data)
    mask = np.zeros((n_users, n_movies))

    for user, movie, pred in zip(train_users, train_movies, train_ratings):
        data[user][movie] = pred
        mask[user][movie] = 1

    data_torch = torch.tensor(data, device=device).float()
    mask_torch = torch.tensor(mask, device=device)

   

    # Obtain reconstruction matrix for refeeding
    if args.refeed_rec_folder:
        reconstruction = np.loadtxt(os.path.join(args.refeed_rec_folder, RECONSTRUCTION_FILE), delimiter=',')
        reconstruction_torch = torch.tensor(reconstruction, device=device).float()
        dataloader =  DataLoader(
            TensorDataset(
                data_torch, 
                mask_torch,
                reconstruction_torch),
        batch_size)
    else:
        dataloader = DataLoader(
            TensorDataset(
            data_torch, 
            mask_torch,
        ), batch_size)
    
    # Logging
    writer = SummaryWriter(args.log_dir)

    # ITERATE
    logging.info("Iterate")
    step = 0
    manager = enlighten.get_manager()
    pbar = manager.counter(total=len(dataloader) * n_epochs)
    for epoch in range(n_epochs):
        for data in dataloader:
            data_batch = data[0]
            mask_batch = data[1]
            optimizer.zero_grad()
            reconstructed_batch = autoencoder(data_batch)
            output = reconstructed_batch.detach()

            loss = loss_function(data_batch, reconstructed_batch, mask_batch)

            loss.backward()

            optimizer.step()
            
            # Refeeding
            optimizer.zero_grad()
            refed_output = autoencoder(data[2]) if args.refeed_rec_folder else autoencoder(output)
            loss = nn.MSELoss()(output, refed_output)
            loss.backward()
            optimizer.step()

            writer.add_scalar('loss', loss, step)
            pbar.update(1)
            step += 1

        if args.no_val_split:
            pbar.desc = 'At epoch {:3d}'.format(epoch)
        elif epoch % show_validation_score_every_epochs == 0:
            reconstructed_matrix = reconstruct_whole_matrix(autoencoder, data_torch, batch_size)
            predictions = extract_prediction_from_full_matrix(reconstructed_matrix, val_users, val_movies)
            reconstuction_rmse = get_score(predictions, val_ratings)
            pbar.desc = 'At epoch {:3d} loss is {:.4f}'.format(epoch, reconstuction_rmse)
            writer.add_scalar('reconstuction_rmse', reconstuction_rmse, step)
    
    with torch.no_grad():
        reconstructed_matrix = reconstruct_whole_matrix(autoencoder, data_torch, batch_size)

        # PREDICT + WRITE OUT TEST SET
        if args.out_folder:
            logging.info('Predict test set')
            test_predictions = extract_prediction_from_full_matrix(reconstructed_matrix,
                                                                test_users, 
                                                                test_movies)
            if args.no_val_split:
                filename = 'svd_test_submission_no_val_split.csv'
            else:
                filename = 'svd_test_submission.csv'                                                                
                with open(os.path.join(args.out_folder, filename), 'w') as f:
                    f.write('Id,Prediction\n')
                    for(user,movie,prediction) in zip (test_users,test_movies,test_predictions):
                        f.write("r{}_c{},{}\n".format(user+1,movie+1,prediction))
        
        if args.no_val_split:
            logging.warning("Cannot write gradient search file or validation predictions when -no_val_split option is set")
        else:
            # WRITE GRADIENT SEARCH FILE
            if args.perform_gs:
                final_val_predictions = extract_prediction_from_full_matrix(reconstructed_matrix, val_users, val_movies)
                final_val_rmse = get_score(final_val_predictions, val_ratings)
                np.savetxt(os.path.join(args.in_folder, 'temp_gs_deepRec.txt'), [final_val_rmse])
            # PREDICT + WRITE OUT VALIDATION SET
            if args.val_folder:
                logging.info('Predict validation set')
                val_predictions = extract_prediction_from_full_matrix(reconstructed_matrix,
                                                                    val_users, 
                                                                    val_movies)
                
                with open(os.path.join(args.val_folder, 'deep_rec_val_submission.csv'), 'w') as f:
                    f.write('Id,Prediction\n')
                    for(user,movie,prediction) in zip (val_users,val_movies,val_predictions):
                        f.write("r{}_c{},{}\n".format(user+1,movie+1,prediction))

        # SAVE MODEL
        if args.save_model_folder:
            logging.info('Save model')
            if not os.path.exists(args.save_model_folder):
                os.makedirs(args.save_model_folder)
            model_file = os.path.join(args.save_model_folder,AUTOENCODER_MODEL_FILE)
            torch.save(autoencoder.state_dict(),model_file)

        """"
        # SAVE RECONSTRUCTED MATRIX
        if args.save_reconstruction_folder:
            logging.info("Save reconstructed matrix")
            save_to_file(reconstructed_matrix, 
                    args.save_reconstruction_folder, 
                    RECONSTRUCTION_FILE)
        """

    logging.info("Finished")
