# Funk SVD reconstruction

Obtained with the following parameters:

- k=10
- lambda_reg=0.1
- lr=0.001
- i=1000

(not setting seed, use default 42)