#!/usr/bin/env python3.7
"""
SVD++

Reference implementation which this mostly is based on can be found
in Geoffry Bolmier's github repository: https://github.com/gbolmier/funk-svd
"""

import os
import logging
import argparse
import numpy as np
from utils import *
from numba import njit
from numba.typed import List

USER_FEATURES_FILE = 'user_features.csv'
MOVIE_FEATURES_FILE = 'movie_features.csv'
USER_BIAS_FILE = 'user_bias.csv'
MOVIE_BIAS_FILE = 'movie_bias.csv'
IMPLICIT_BIAS_FILE = 'implicit_bias.csv'

@njit # njit can be very helpful (google it), see reference for what python/numpy constructs are supported
def initialize(n_features, seed):
    np.random.seed(seed)
    user_features = np.random.normal(0, .1, (n_users, n_features))
    movie_features = np.random.normal(0, .1, (n_movies, n_features))
    implicit_bias = np.zeros((n_features, n_movies))
    user_bias = np.zeros(n_users)
    movie_bias = np.zeros(n_movies)
    return user_bias, movie_bias, implicit_bias, user_features, movie_features

@njit
def iterate(train_users, train_movies, train_ratings,
            mean_rating, user_bias, movie_bias, implicit_bias,
            movies_rated, user_features, movie_features,
            const_implicit, bias_learning_rate, learning_rate,
            bias_lambda_reg, lambda_reg):
    train_residuals = np.zeros(len(train_ratings))
    implicit_bias_sum = np.zeros_like(user_features)
    for user in range(user_features.shape[0]):
        for feature in range(user_features.shape[1]):
            for movie_rated in movies_rated[user]:
                implicit_bias_sum[user,feature] += implicit_bias[feature,movie_rated]
            implicit_bias_sum[user,feature] *= const_implicit[user]
    for i,(user,movie,rating) in enumerate(zip(train_users,train_movies,train_ratings)):
        prediction = mean_rating + user_bias[user] + movie_bias[movie]
        for feature in range(user_features.shape[1]):
            prediction += movie_features[movie, feature]\
                 * (user_features[user, feature] + implicit_bias_sum[user][feature])
        residual = rating - prediction
        train_residuals[i] = residual
        user_bias[user] += bias_learning_rate * (residual - bias_lambda_reg * user_bias[user])
        movie_bias[movie] += bias_learning_rate * (residual - bias_lambda_reg * movie_bias[movie])
        for feature in range(user_features.shape[1]):
            user_feature = user_features[user, feature]
            movie_feature = movie_features[movie, feature]
            user_features[user, feature] += learning_rate * \
                                    (residual * movie_feature - lambda_reg * user_feature)
            movie_features[movie, feature] += learning_rate * \
                                    (residual * (user_feature + implicit_bias_sum[user,feature]) \
                                         - lambda_reg * movie_feature)
            for movie_rated in movies_rated[user]:
                implicit_bias[feature,movie_rated] += learning_rate \
                    * (residual * const_implicit[user]*movie_feature \
                        - lambda_reg*implicit_bias[feature,movie_rated])
    train_rmse = np.sqrt(np.square(train_residuals).mean())
    return user_bias, movie_bias, user_features, movie_features, implicit_bias, train_rmse

@njit
def get_validation_rmse(val_users, val_movies, val_ratings,
            mean_rating, user_bias, movie_bias, implicit_bias,
            movies_rated, user_features, movie_features, const_implicit):
    implicit_bias_sum = np.zeros_like(user_features)
    for user in range(user_features.shape[0]):
        for feature in range(user_features.shape[1]):
            for movie_rated in movies_rated[user]:
                implicit_bias_sum[user,feature] += implicit_bias[feature,movie_rated]
            implicit_bias_sum[user,feature] *= const_implicit[user]
    val_residuals = np.zeros(len(val_ratings))
    for i, (user, movie, rating) in enumerate(zip(val_users, val_movies, val_ratings)):
        prediction = mean_rating + user_bias[user] + movie_bias[movie]
        for feature in range(user_features.shape[1]):
            prediction += movie_features[movie, feature]\
                 * (user_features[user, feature] + implicit_bias_sum[user, feature])
        val_residuals[i] = rating - prediction
    return np.sqrt(np.square(val_residuals).mean())

def save_model(user_features, 
                movie_features,
                user_bias, movie_bias,
                implicit_bias,
                folder):
    save_to_file(user_features,folder,USER_FEATURES_FILE)
    save_to_file(movie_features,folder,MOVIE_FEATURES_FILE)
    save_to_file(user_bias,folder,USER_BIAS_FILE)
    save_to_file(movie_bias,folder,MOVIE_BIAS_FILE)
    save_to_file(implicit_bias,folder,IMPLICIT_BIAS_FILE)

if __name__ == "__main__":
    # PARSE ARGUMENTS
    parser = argparse.ArgumentParser()
    parser.add_argument('-k',
                       type=int,
                       help='number of features',
                       default=10)
    parser.add_argument('-l_reg_bias',
                       type=float,
                       help='regularizer lambda for user/movie bias terms',
                       default=0.0025)
    parser.add_argument('-l_reg',
                       type=float,
                       help='regularizer lambda for features and implicit bias',
                       default=0.0075)
    parser.add_argument('-lr_bias',
                        type=float,
                        help='learning rate for user/movie bias terms',
                        default=0.0001)
    parser.add_argument('-lr',
                        type=float,
                        help='learning rate for features and implicit bias',
                        default=0.0001)
    parser.add_argument('-i',
                        type=int,
                        help='number of iterations',
                        default=19)
    parser.add_argument('-seed',
                        type=int,
                        help='set seed for reproducibility',
                        default=DEFAULT_SEED)
    parser.add_argument('-preinit_folder',
                        type=validate_file,
                        help="preinitialization files folder")
    parser.add_argument('-save_model_folder',
                        help="path to folder where model should be saved")
    #parser.add_argument('-save_reconstruction_folder',
    #                    help="folder where to save reconstruction")
    parser.add_argument("-in_folder",
                        type=validate_file,
                        help="input data folder", 
                        default='Input/')
    parser.add_argument("-out_folder",
                        type=validate_file,
                        help="output folder", 
                        default='Output/')
    parser.add_argument("-test_folder",
                        type=validate_file,
                        help="output folder", 
                        default='Output/test_pred')
    parser.add_argument("-val_folder",
                        type=validate_file,
                        help="output folder", 
                        default='Output/validation_pred')
    parser.add_argument("-no_val_split",
                        help="train on full data set, don't split for validation",
                        action='store_true')
    parser.add_argument("-perform_gs",
                        type=bool,
                        help="generates file for gs", 
                        default=False)
    parser.add_argument('-v', '--verbose',
                        help="displays output (logging at info level)",
                        action="store_const", 
                        dest="loglevel", 
                        const=logging.INFO,
                        default=logging.CRITICAL)
    args = parser.parse_args()
    n_features = args.k
    bias_lambda_reg = args.l_reg_bias
    lambda_reg = args.l_reg
    bias_learning_rate = args.lr_bias
    learning_rate = args.lr
    n_iterations = args.i
    logging.getLogger().setLevel(args.loglevel)

    logging.info("Starting")

    # READ DATA IN
    train_pd, val_pd, test_pd = get_split_dataset(args.in_folder, args.seed, args.no_val_split)
    train_users, train_movies, train_ratings = extract_users_items_predictions(train_pd)
    val_users, val_movies, val_ratings = extract_users_items_predictions(val_pd)
    test_users, test_movies, _ = extract_users_items_predictions(test_pd)

    # INITIALIZE
    logging.info("Initialize")
    mean_rating = train_ratings.mean()
    user_bias, movie_bias, implicit_bias, user_features, movie_features \
        = initialize(n_features, args.seed)
    if args.preinit_folder:
        precomp_n_features = 0
        with open(os.path.join(args.preinit_folder, USER_FEATURES_FILE)) as f:
            first_line = f.readline()
            precomp_n_features = len(first_line.split(','))
        min_n_features = min(precomp_n_features, n_features)
        user_features[0:,0:min_n_features] = np.loadtxt(os.path.join(args.preinit_folder, USER_FEATURES_FILE), delimiter=',')[0:,0:min_n_features]
        movie_features[0:,0:min_n_features] = np.loadtxt(os.path.join(args.preinit_folder, MOVIE_FEATURES_FILE), delimiter=',')[0:,0:min_n_features]
        user_bias = np.loadtxt(os.path.join(args.preinit_folder, USER_BIAS_FILE), delimiter=',')
        movie_bias = np.loadtxt(os.path.join(args.preinit_folder, MOVIE_BIAS_FILE), delimiter=',')
    n_implicit = np.zeros(n_users, dtype=np.int32)
    for user in train_users:
        n_implicit[user]+=1
    movies_rated = List()
    for n in n_implicit:
        movies_rated.append(np.zeros(n, dtype=np.int32))
    movies_rated_idx = np.zeros_like(n_implicit, dtype=np.int32)
    for user, movie in zip(train_users, train_movies):
        movies_rated[user][movies_rated_idx[user]] = movie
        movies_rated_idx[user]+=1
    const_implicit = 1 / np.sqrt(n_implicit)

    # ITERATE
    logging.info("Iterate")
    for i in range(n_iterations):
        user_bias, movie_bias, user_features, movie_features, implicit_bias, train_rmse = \
                iterate(train_users, train_movies, train_ratings,
                        mean_rating, user_bias, movie_bias, implicit_bias,
                        movies_rated, user_features, movie_features,
                        const_implicit, bias_learning_rate, learning_rate,
                        bias_lambda_reg, lambda_reg)
        if args.no_val_split:
            logging.info("Iteration {}/{}".format(i+1, n_iterations))
        elif args.loglevel < logging.CRITICAL:
            val_rmse = get_validation_rmse(val_users, val_movies, val_ratings,
                                mean_rating, user_bias, movie_bias, implicit_bias,
                                movies_rated, user_features, movie_features, const_implicit)
            logging.info("Iteration {}/{}:\t[ train rmse: {:.4f}, validation rmse: {:.4f} ]" \
                .format(i+1, n_iterations, train_rmse, val_rmse))


    # PREDICT + WRITE OUT TEST SET
    implicit_bias_sum = np.zeros((n_users, n_features))
    for user in range(n_users):
        for movie_rated in movies_rated[user]:
            implicit_bias_sum[user,:] += implicit_bias[:,movie_rated]
        implicit_bias_sum[user,:] *= const_implicit[user]

    if args.test_folder:
        logging.info('Predict test set')
        if args.no_val_split:
            filename = 'svdpp_test_submission_no_val_split.csv'
        else:
            filename = 'svdpp_test_submission.csv'
        with open(os.path.join(args.test_folder,filename), 'w') as f:
            f.write('Id,Prediction\n')
            for(user,movie) in zip (test_users,test_movies):
                prediction = mean_rating + user_bias[user] + movie_bias[movie] \
                            + np.dot(movie_features[movie], (user_features[user] \
                            + implicit_bias_sum[user,:]))
                prediction = min(max(1, prediction), 5) # clip
                f.write("r{}_c{},{}\n".format(user+1,movie+1,prediction))
    
    if args.no_val_split:
        logging.warning("Cannot write gradient search file or validation predictions when -no_val_split option is set")
    else:
        # WRITE GRADIENT SEARCH FILE
        if args.perform_gs:
            final_val_rmse = get_validation_rmse(val_users, val_movies, val_ratings,
                                mean_rating, user_bias, movie_bias, implicit_bias,
                                movies_rated, user_features, movie_features, const_implicit)
            np.savetxt(os.path.join(args.in_folder, 'temp_gs_svdpp.txt'), [final_val_rmse])
        # PREDICT + WRITE OUT VALIDATION SET
        if args.val_folder:
            logging.info("Predict validation set")
            with open(os.path.join(args.val_folder, 'svdpp_val_submission.csv'), 'w') as f:
                f.write('Id,Prediction\n')
                for(user,movie,rating) in zip (val_users,val_movies, val_ratings):
                    prediction = mean_rating + user_bias[user] + movie_bias[movie] \
                                + np.dot(movie_features[movie], (user_features[user] \
                                + implicit_bias_sum[user,:]))
                    prediction = min(max(1, prediction), 5) # clip
                    f.write("r{}_c{},{}\n".format(user+1,movie+1,prediction))
    
    # SAVE MODEL
    if args.save_model_folder:
        logging.info('Save model')
        save_model(user_features, 
                    movie_features,
                    user_bias, 
                    movie_bias,
                    implicit_bias,
                    args.save_model_folder)

    """
    # SAVE RECONSTRUCTED MATRIX
    if args.save_reconstruction_folder:
        logging.info("Save reconstructed matrix")
        reconstruction = (user_features+implicit_bias_sum) @ movie_features.T \
                        + np.expand_dims(user_bias,1)  \
                        + np.expand_dims(movie_bias,0) \
                        + mean_rating
        reconstruction = np.where(reconstruction > 5, 5, reconstruction)
        reconstruction = np.where(reconstruction < 1, 1, reconstruction)
        save_to_file(reconstruction, 
                    args.save_reconstruction_folder, 
                    RECONSTRUCTION_FILE)
    """

    logging.info("Finished")
