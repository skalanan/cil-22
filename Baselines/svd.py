#!/usr/bin/env python3.7
"""
SVD Baseline - As developed for Problem 3 of Homework 4 of the CIL course.
"""

import os
import logging
import argparse
import numpy as np
from utils import *

if __name__ == "__main__":
    # PARSE ARGUMENTS
    parser = argparse.ArgumentParser()
    parser.add_argument('-k',
                       type=int,
                       help='number of features',
                       default=10)
    parser.add_argument('-seed',
                        type=int,
                        help='set seed for reproducibility',
                        default=DEFAULT_SEED)
    parser.add_argument('-preinit_folder',
                        type=validate_file,
                        help="preinitialization files folder")
    parser.add_argument('-save_model_folder',
                        help="path to folder where model should be saved")
    #parser.add_argument('-save_reconstruction_folder',
    #                    help="folder where to save reconstruction")
    parser.add_argument("-in_folder",
                        type=validate_file,
                        help="input data folder", 
                        default='Input/')
    parser.add_argument("-out_folder",
                        type=validate_file,
                        help="output folder", 
                        default='Output/test_pred')
    parser.add_argument("-val_folder",
                        type=validate_file,
                        help="output folder", 
                        default='Output/validation_pred')
    parser.add_argument("-no_val_split",
                        help="train on full data set, don't split for validation",
                        action='store_true')
    parser.add_argument("-perform_gs",
                        type=bool,
                        help="generates file for gs", 
                        default=False)
    parser.add_argument('-v', '--verbose',
                        help="displays output (logging at info level)",
                        action="store_const", 
                        dest="loglevel", 
                        const=logging.INFO,
                        default=logging.CRITICAL)             
    args = parser.parse_args()
    n_features = args.k
    logging.getLogger().setLevel(args.loglevel)

    logging.info('Starting')

    # READ DATA IN
    train_pd, val_pd, test_pd = get_split_dataset(args.in_folder, args.seed, args.no_val_split)
    train_users, train_movies, train_ratings = extract_users_items_predictions(train_pd)
    val_users, val_movies, val_ratings = extract_users_items_predictions(val_pd)
    test_users, test_movies, _ = extract_users_items_predictions(test_pd)

    # INITIALIZE
    logging.info("Initialize")
    data = np.full((n_users, n_movies), np.nan)
    for user, movie, rating in zip(train_users, train_movies, train_ratings):
        data[user][movie] = rating

    # SVD MATRIX RECONSTRUCTIOM
    logging.info("SVD matrix reconstruction")
    imputed_matrix = np.where(np.isnan(data), np.ma.array(data, mask=np.isnan(data)).mean(axis=0), data) 
    number_of_singular_values = min(n_users, n_movies)
    assert(n_features <= number_of_singular_values), "k is greater than number of singular values"
    U, s, Vt = np.linalg.svd(imputed_matrix, full_matrices=False)
    S_sqrt = np.zeros((n_features, n_features))
    S_sqrt[:, :] = np.diag(np.sqrt(s[:n_features]))
    U_truncated = np.zeros((imputed_matrix.shape[0], n_features));
    Vt_truncated = np.zeros((n_features, imputed_matrix.shape[1]))
    U_truncated[:,:] = U[:, :n_features]
    Vt_truncated[:,:] = Vt[:n_features, :] 
    U_ = U_truncated @ S_sqrt
    Vt_ = S_sqrt @ Vt_truncated

    predict = lambda i, j: U_[i,:].dot(Vt_[:,j])
    if args.no_val_split:
        logging.warning("Cannot write gradient search file or validation predictions when -no_val_split option is set")
    else:
        val_predictions = np.zeros(len(val_users))
        for i,(user,movie) in enumerate(zip(val_users, val_movies)):
            val_predictions[i] = predict(user, movie)
        final_val_rmse = get_score(val_predictions, val_ratings)
        logging.info("Validation loss: {:.4f}".format(final_val_rmse))

        # WRITE GRADIENT SEARCH FILE
        if args.perform_gs:
            np.savetxt(os.path.join(args.in_folder, 'temp_gs_svd.txt'), [final_val_rmse])
        
        
        # PREDICT + WRITE OUT VALIDATION SET
        if args.val_folder:
                logging.info('Predict validation set')
                with open(os.path.join(args.val_folder, 'svdpp_val_submission.csv'), 'w') as f:
                    f.write('Prediction\n')
                    for(user,movie,rating) in zip (val_users,val_movies, val_ratings):
                        f.write("{}\n".format(predict(user,movie)))

    
    # PREDICT + WRITE OUT TEST SET
    if args.out_folder:
        logging.info('Predict test set')
        if args.no_val_split:
            filename = 'svdpp_test_submission_no_val_split.csv'
        else:
            filename = 'svdpp_test_submission.csv'
        with open(os.path.join(args.out_folder,filename), 'w') as f:
            f.write('Id,Prediction\n')
            for(user,movie) in zip (test_users,test_movies):
                f.write("r{}_c{},{}\n".format(user+1,movie+1,predict(user,movie)))

    # SAVE MODEL
    if args.save_model_folder:
        logging.info('Save model')
        save_to_file(U_, args.save_model_folder)
        save_to_file(Vt_,args.save_model_folder)
    
    """
    # SAVE RECONSTRUCTED MATRIX
    if args.save_reconstruction_folder:
        logging.info("Save reconstructed matrix")
        save_to_file(U_@Vt_, 
                args.save_reconstruction_folder, 
                RECONSTRUCTION_FILE)
    """

    logging.info("Finished")
