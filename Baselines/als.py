#!/usr/bin/env python3.7
"""
Alternating Least Squares (PySpark)
"""

import os
import logging
import argparse
import pandas as pd
import numpy as np
from utils import *
import findspark
findspark.init()
from pyspark.sql import SparkSession
from pyspark.ml.recommendation import ALS

ALS_MODEL_FILE = 'alsModel'

def write_predictions(predictions, folder, filename):
    with open(os.path.join(folder,filename), 'w') as f:
            f.write('Id,Prediction\n')
            for _, row in predictions.toPandas().iterrows():
                f.write("r{:0.0f}_c{:0.0f},{}\n".format(row["User"]+1,row["Movie"]+1,row["prediction"]))

if __name__ == "__main__":
    # PARSE ARGUMENTS
    parser = argparse.ArgumentParser()
    parser.add_argument('-k',
                       type=int,
                       help='number of features',
                       default=10)
    parser.add_argument('-l_reg',
                       type=float,
                       help='regularizer lambda',
                       default=0.1)
    parser.add_argument('-i',
                        type=int,
                        help='number of iterations',
                        default=20)
    parser.add_argument('-seed',
                        type=int,
                        help='set seed for reproducibility',
                        default=DEFAULT_SEED)  
    parser.add_argument('-preinit_folder',
                        type=validate_file,
                        help="preinitialization files folder")
    parser.add_argument('-save_model_folder',
                        help="path to folder where model should be saved")
    #parser.add_argument('-save_reconstruction_folder',
    #                    help="folder where to save reconstruction")
    parser.add_argument("-in_folder",
                        type=validate_file,
                        help="input data folder", 
                        default='Input/')
    parser.add_argument("-out_folder",
                        type=validate_file,
                        help="output folder", 
                        default='Output/')
    parser.add_argument("-val_folder",
                        type=validate_file,
                        help="output folder", 
                        default='Output/validation_pred')
    parser.add_argument("-no_val_split",
                        help="train on full data set, don't split for validation",
                        action='store_true')
    parser.add_argument("-perform_gs",
                        type=bool,
                        help="generates file for gs", 
                        default=False)
    parser.add_argument('-v', '--verbose',
                        help="displays output (logging at info level)",
                        action="store_const", 
                        dest="loglevel", 
                        const=logging.INFO,
                        default=logging.CRITICAL)
    args = parser.parse_args()
    n_features = args.k
    lambda_reg = args.l_reg
    n_iterations = args.i
    logging.getLogger().setLevel(args.loglevel)

    logging.info("Starting")

    # READ DATA IN
    train_pd, val_pd, test_pd = get_split_dataset(args.in_folder, args.seed, args.no_val_split)
    train_users, train_movies, train_ratings = extract_users_items_predictions(train_pd)
    val_users, val_movies, val_ratings = extract_users_items_predictions(val_pd)
    test_users, test_movies, _ = extract_users_items_predictions(test_pd)

    # INITIALIZE
    logging.info("Initialize")

    # Set seed for reproducibility
    np.random.seed(args.seed)

    # Create spark session
    spark = SparkSession.builder\
        .master("local")\
        .appName("ALS")\
        .config('spark.ui.port', '4050')\
        .getOrCreate()
    spark.sparkContext.setLogLevel("OFF")

    # Transform data
    train_df = pd.DataFrame({'User': train_users,
                             'Movie': train_movies,
                             'Rating': train_ratings })
    test_df = pd.DataFrame({'User': test_users,
                            'Movie': test_movies })

    train_sdf = spark.createDataFrame(train_df)
    test_sdf = spark.createDataFrame(test_df)
    
    # Initialize model
    als = ALS(rank = n_features,
          maxIter=n_iterations, 
          regParam=lambda_reg, 
          userCol="User", 
          itemCol="Movie", 
          ratingCol="Rating",
          coldStartStrategy="drop",
          seed=args.seed)

    if args.preinit_folder:
        als.load(os.join(args.preinit_folder, ALS_MODEL_FILE))

    # ITERATE
    logging.info("Iterate")
    model = als.fit(train_sdf)

    if args.no_val_split:
            logging.warning("Cannot write gradient search file or validation predictions when -no_val_split option is set")
    else:
        val_df = pd.DataFrame({'User': val_users,
                           'Movie': val_movies })
        val_sdf = spark.createDataFrame(val_df)
        val_predictions = model.transform(val_sdf)
        # CALCULATE VALIDATION LOSS
        final_val_rmse = get_score(val_predictions.toPandas()['prediction'].to_numpy(), val_ratings)
        logging.info("Validation rsme: %f",final_val_rmse)
        # WRITE GRADIENT SEARCH FILE
        if args.perform_gs:
            np.savetxt(os.path.join(args.in_folder, 'temp_gs_als.txt'), [final_val_rmse])
        # PREDICT + WRITE OUT VALIDATION SET
        if args.val_folder and not args.no_val_split:
            logging.info('Predict validation set')
            write_predictions(val_predictions, args.val_folder, 'als_val_submission.csv')

    # PREDICT + WRITE OUT TEST SET
    if args.out_folder:
        logging.info('Predict test set')
        test_predictions = model.transform(test_sdf)
        write_predictions(test_predictions, args.out_folder, 'als_test_submission.csv')

    # SAVE MODEL
    if args.save_model_folder:
        logging.info('Save model')
        #if not os.path.exists(args.save_model_folder):
        #    os.makedirs(args.save_model_folder)
        als.save(os.join(args.save_model_folder, ALS_MODEL_FILE))
    
    """
    # SAVE RECONSTRUCTED MATRIX
    if args.save_reconstruction_folder:
        logging.info("Save reconstructed matrix")
        all_df = pd.DataFrame({'User': sum([n_movies*[i] for i in range(n_users)], []),
                            'Movie': n_users*[i for i in range(n_movies)] })
        all_sdf = spark.createDataFrame(all_df)
        all_predicted = model.transform(all_sdf)
        reconstructed_matrix = all_predicted \
                                .toPandas()['prediction'] \
                                .to_numpy() \
                                .reshape((n_users, n_movies))
        save_to_file(reconstructed_matrix, 
                args.save_reconstruction_folder, 
                RECONSTRUCTION_FILE)
    """

    logging.info("Finished")
