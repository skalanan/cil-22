# ALS (PySpark)

[Link](https://spark.apache.org/docs/latest/api/python/reference/api/pyspark.ml.recommendation.ALS.html) to documentation.

Requires a Java and Spark installation.
Java should be be version 11 which in fact worked for me with some warnings (or 8).
To install Spark run chmod +x install.sh, then ./install.sh and finally source ~/.profile.

---
# SVD

Basic linear algebra SVD on matrix imputed with movie mean. 
Code as developed for problem 3 of homework 4 of the CIL course.

---

# FunkSVD

![funk svd formula](../Images/funkSVD.png)

Original idea described in: https://sifter.org/~simon/journal/20061211.html

Reference implementation which this is based on: https://github.com/gbolmier/funk-svd

Useful Quora answer on the difference between linear algebra SVD, Funk SVD and SVD++: https://qr.ae/pvPIst

Paper from tutorial 5 slides: https://www.cs.uic.edu/~liub/KDD-cup-2007/proceedings/Regular-Paterek.pdf

*For 10,000 takes ~10min*

*Looking at the repo's code might be easier for understanding at first*

---

# SVD++

![SVD++ objective](../Images/SVDpp_obj.png)
![SVD++ stochastic gradient descent](../Images/SVDpp_sdg.png)

SVD++ improvement on top of Funk SVD.
N(u) is the set of all items for which a user u has provided implicit feedback taking the action of rating it.

This approach has been described in [Factorization Meets the Neighborhood: a Multifaceted
Collaborative Filtering Model](https://people.engr.tamu.edu/huangrh/Spring16/papers_course/matrix_factorization.pdf) by Yehuda Koren in 2008.

Useful Quora answer on the difference between linear algebra SVD, Funk SVD and SVD++: https://qr.ae/pvPIst

Reference implementation which this is mostly based on: https://github.com/gbolmier/funk-svd

---

# Weighted SVD++

A combination of weighted SVD and SVD++.

Original Idea of weighted SVD described in [Weighted-SVD: Matrix Factorization with Weights
on the Latent Factors](https://arxiv.org/pdf/1710.00482.pdf) by Hung-Hsuan Chen.

Reference implementation which this is mostly based on: https://github.com/gbolmier/funk-svd

---

# DeepRec

Autoencoder implementation based on the paper [Training Deep AutoEncoders for Recommender Systems](https://arxiv.org/pdf/1708.01715.pdf) by Oleksii Kuchaiev and Boris Ginsburg.

- Layers: (n, 64, 128, 128, dp(0.55), 128, n)
- One single refeeding step
- No weight tying between decoder to encoder

---
---

*Image credit: [Factorization Meets the Neighborhood: a Multifaceted
Collaborative Filtering Model](https://people.engr.tamu.edu/huangrh/Spring16/papers_course/matrix_factorization.pdf) by Yehuda Koren in 2008*