*Project: (1) Collaborative Filtering, Kaggle Team Name: SeStYvTi*

---
# Hybrid Collaborative Filtering for Recommender Systems

## Description

This repositoriy contains the submission of team *SeStYvTi* for *Project (1) collaborative filtering* of the ETH CIL course.

### Approaches 
For our solution we considered the following approaches:

* SVD
* FunkSVD
* SVD++
* weighted SVD++
* RANSAC
* ALS
* DeepRec
* Ensemble

We have implemented all of them as python scripts.
All implementations can be found the Baselines folder, except for the Ensemble which is in its own folder.

### Repository structure
The repository is structured as follows:

<pre>
cil-22
│   README.md
│   install.sh  
|   requirements.txt
|   gitignore
|   gen_pred_sets.py        # Script for creating writing out validation / train set split data sets
│
|───Baselines               # Contains scripts implementing baselines
│   │   svd.py
│   │   funkSVD.py
|   |   SVDpp.py                 
|   |   wSVDpp.py
|   |   als.py
|   |   deepRec.py
|   |   utils.py
|   |   README.md
|   |   LICENSE
│   │
│   |───Preinitialization   # Used by baselines for preinitialization
│   |   │───FunkSVD───── ...
│   |   │───Autoencoder─ ...
│   |   └───wSVDpp────── ...
|   |
|   └───Reconstruction
|       └───...
│   
│───GridSearch              # Bash scripts to perform grid search on baselines
│    │   file021.txt
│    │   file022.txt
│
│───ransac
│───Input                   # Data set / input needed by scripts
|   | data_train.csv
|   | sampleSubmission.csv 
└───Output                  # Folder containing submissions generated
│   │───ensemble_pred───── ... # Folder containing ensemble predictions
│   │───grid_search───── ... # Folder containing gridsearch predictions
│   │───test_pred───── ...
│   |   │───SVDpp+FunkSVD+ALS+WSVD++───── ... # Folder containing Test Predictions trained with train-validation split
│   │───validation_pred───── ...
│   |   │───SVDpp+FunkSVD+ALS+WSVD++───── ... # Folder containing Validation Predictions trained with train-validation split

</pre>

### Final submission 

Following command can be executed to produce the test submission file with test score: 0.97499 using the already provided .csv files for blending:

    python3 Ensemble/ensemble.py

The Test Submission file can be found under the following path: Output/ensemble_pred/ensemble_SVDpp+FunkSVD+ALS+WSVD++.csv

At the time we wrote the report we seemed to get consistent results. Unfortunately, after the report was written, when we tried to reproduce the score of our final submission, the ensemble of SVD++ w/ FunkSVD, FunkSVD, ALS and WSVD++ we were only able to reproduce a score of 0.97608 instead of 0.97499: 

The score can be reproduced as follow: 
    
    python3 Baselines/funkSVD.py -k 15 -l_reg 0.1 -lr 0.001 -i 1500 -out_folder Output/test_pred/SVDpp+FunkSVD+ALS+WSVD++ -val_folder Output/validation_pred/SVDpp+FunkSVD+ALS+WSVD++ -save_model_folder Baselines/Preinitialization/FunkSVD
    python3 Baselines/SVDpp.py -k 15 -l_reg_bias 0.01 -l_reg 0.0015 -lr_bias 0.001 -lr 0.0001 -i 23 -out_folder Output/test_pred/SVDpp+FunkSVD+ALS+WSVD++ -val_folder Output/validation_pred/SVDpp+FunkSVD+ALS+WSVD++ -preinit_folder Baselines/Preinitialization/FunkSVD
    python3 Baselines/als.py -k 5 -l_reg 0.01 -i 15 -out_folder Output/test_pred/SVDpp+FunkSVD+ALS+WSVD++ -val_folder Output/validation_pred/SVDpp+FunkSVD+ALS+WSVD++
    python3 Baselines/wSVDpp.py -k 15 -l_reg_bias 0.01 -l_reg 0.0075 -lr_bias 0.00001 -lr 0.00001 -i 700 -out_folder Output/test_pred/SVDpp+FunkSVD+ALS+WSVD++ -val_folder Output/validation_pred/SVDpp+FunkSVD+ALS+WSVD++ -preinit_folder Baselines/Preinitialization/FunkSVD

    python3 Ensemble/ensemble.py -alpha 0

## Installation

### Prerequisites

* Python version>=3.7
* Java installation version>=8
* (Spark version 3.1.3 with Hadoop 2.7)

### Required Python Packages

Install all the python packages this repository requires via:

    pip install -r requirements.txt 
    

### Spark

Spark is only required to run the ALS baseline. In case, you don't have Spark, install it entering the following commands from the terminal, after navigating to this projects root folder: 

    chmod  +x ./install.sh
    ./install.sh
    source ~/.profile

## Authors and acknowledgment

### Authors
* Yves Kempter
* Senthuran Kalananthan
* Radenko Tanasic
* Stephanie Ruhstaller

### Co-Authors
Since used, extended and modified the code of Geoffry Bolmier's  [FunkSVD implementation](https://github.com/gbolmier/funk-svd) on Github, many thanks go to that repository's authors, which we list here as co-contributors:
* Geoffry Bolmier
* [@sicotfre](https://github.com/sicotfre) (on Github)
* Max Halford
* Joao Felipe Guedes

## License
 MIT 

(License of Geoffry Bolmier's [FunkSVD implementation](https://github.com/gbolmier/funk-svd) which served as a basis for many of our baselines)
